import React from "react";
import Calculator from "./calculatorComponents/Calculator";

const CalculatorApp = () => {
  return <Calculator />;
};

export default CalculatorApp;
