import React from "react";
import ReactDOM from "react-dom";
import "./index.css";

// Counter App
// import App from "./App";

// ForexApp
// import ForexApp from "./ForexApp";

// CalculatorApp
import CalculatorApp from "./CalculatorApp";

import * as serviceWorker from "./serviceWorker";

// bootstrap
import "bootstrap/dist/css/bootstrap.css";

ReactDOM.render(
  <React.StrictMode>
    {/* <ForexApp /> */}
    <CalculatorApp />
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
