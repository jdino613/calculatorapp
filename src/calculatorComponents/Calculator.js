import React, { useState } from "react";
import FirstInput from "./FirstInput";
import SecondInput from "./SecondInput";
import Buttons from "./Buttons";
import Button from "./Button";

const Calculator = () => {
  const [firstAmount, setFirstAmount] = useState(0);
  const [secondAmount, setSecondAmount] = useState(0);
  let [result, setResult] = useState(0);

  const handleFirstAmount = e => {
    setFirstAmount(e.target.value);
    console.log(firstAmount);
  };

  const handleSecondAmount = e => {
    setSecondAmount(e.target.value);
    console.log(secondAmount);
  };

  const handleAdd = () => {
    setResult((result = parseInt(firstAmount) + parseInt(secondAmount)));
    console.log(result);
  };

  const handleMinus = () => {
    setResult((result = parseInt(firstAmount) - parseInt(secondAmount)));
    console.log(result);
  };

  const handleMultiply = () => {
    setResult((result = parseInt(firstAmount) * parseInt(secondAmount)));
    console.log(result);
  };

  const handleDivide = () => {
    setResult((result = parseInt(firstAmount) / parseInt(secondAmount)));
    console.log(result);
  };

  return (
    <div className="d-flex justify-content-center align-items-center vh-100 bg-secondary flex-column ">
      <FirstInput onChange={handleFirstAmount} />
      <Buttons
        handleAdd={handleAdd}
        handleMinus={handleMinus}
        handleMultiply={handleMultiply}
        handleDivide={handleDivide}
      />
      <SecondInput onChange={handleSecondAmount} />
      {/* <Button 
                class='btn btn-info m-2'
                text = {'='}
                // onClick = {props.handleDivide}
            /> */}
      <div className="m-5">{result ? <h2>Result: {result}</h2> : ""}</div>
    </div>
  );
};

export default Calculator;
