import React from "react";
import { Input } from "reactstrap";

const SecondInput = ({ onChange }) => {
  return <Input className="w-50" onChange={onChange} />;
};

export default SecondInput;
