import React from "react";
import Button from "./Button";

const Buttons = props => {
  return (
    <div className="d-flex flex-row m-5">
      <Button class="btn btn-info m-5" text={"+"} onClick={props.handleAdd} />
      <Button class="btn btn-info m-5" text={"-"} onClick={props.handleMinus} />
      <Button
        class="btn btn-info m-5"
        text={"/"}
        onClick={props.handleDivide}
      />
      <Button
        class="btn btn-info m-5"
        text={"*"}
        onClick={props.handleMultiply}
      />
    </div>
  );
};

export default Buttons;
