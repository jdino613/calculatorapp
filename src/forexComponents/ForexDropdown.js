import React, { useState } from "react";
import {
  FormGroup,
  Label,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from "reactstrap";
import Currencies from "./ForexData";

const ForexDropdown = props => {
  const [dropdownIsOpen, setDropDownIsOpen] = useState(false);
  const [currency, setCurrency] = useState(null);

  return (
    <FormGroup>
      <Label>{props.label}</Label>
      <Dropdown
        isOpen={dropdownIsOpen}
        toggle={() => setDropDownIsOpen(!dropdownIsOpen)}
      >
        <DropdownToggle caret>
          {!props.currency ? "Choose Currency" : props.currency.currency}
        </DropdownToggle>
        <DropdownMenu>
          {Currencies.map((currency, index) => (
            <DropdownItem key={index} onClick={() => props.onClick(currency)}>
              {currency.currency}
            </DropdownItem>
          ))}
        </DropdownMenu>
      </Dropdown>
    </FormGroup>
  );
};

export default ForexDropdown;

// Class
// import React, { Component } from 'react';
// import {FormGroup,Label,Dropdown,DropdownToggle,DropdownMenu,DropdownItem} from 'reactstrap'
// import Currencies from './ForexData'

// class ForexDropdown extends Component {

//     state = {
//         dropdownIsOpen: false,
//         currency: null
//      }

//     render() {
//         console.log(this.props.tables)
//         return (
//             <FormGroup>
//                 <Label>{this.props.label}</Label>
//                 <Dropdown
//                     isOpen= { this.state.dropdownIsOpen }
//                     toggle = { () => this.setState({
//                         dropdownIsOpen: !this.state.dropdownIsOpen
//                     })}
//                 >
//                     <DropdownToggle caret>{!this.props.currency
//                     ?"Choose Currency"
//                     : this.props.currency.currency
//                     }</DropdownToggle>
//                     <DropdownMenu>
//                         {Currencies.map((currency,index)=>(
//                             <DropdownItem
//                                 key = {index}
//                                 onClick = { ()=> this.props.onClick(currency) }
//                             >
//                             {currency.currency}
//                             </DropdownItem>
//                         ))}
//                     </DropdownMenu>
//                 </Dropdown>
//             </FormGroup>
//          );
//     }
// }

// export default ForexDropdown;
