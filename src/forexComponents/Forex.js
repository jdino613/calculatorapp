import React, { useState } from "react";
import ForexDropdown from "./ForexDropdown";
import ForexInput from "./ForexInput";
import ForexRates from "./ForexRates";
import ForexTable from "./ForexTable";
import { Button } from "reactstrap";

const Forex = () => {
  const [amount, setAmount] = useState(0);
  const [baseCurrency, setBaseCurrency] = useState(null);
  const [targetCurrency, setTargetCurrency] = useState(null);
  const [convertedAmount, setConvertedAmount] = useState(0);
  const [convertedCode, setConvertedCode] = useState(null);
  const [showTable, setShowTable] = useState(false);
  const [rates, setRates] = useState([]);

  const handleAmount = e => {
    // this.setState({
    //     amount:e.target.value
    // })
    setAmount(e.target.value);
  };

  const handleBaseCurrency = currency => {
    // this.setState({
    //     baseCurrency: currency
    // })

    const code = currency.code;
    fetch("https://api.exchangeratesapi.io/latest?base=" + code)
      .then(res => res.json())
      .then(res => {
        const rateArray = Object.entries(res.rates);
        // console.log(rateArray)
        setRates(rateArray);
        setShowTable(true);
      });
    setBaseCurrency(currency);
  };

  const handleTargetCurrency = currency => {
    // this.setState({
    //     targetCurrency: currency
    // })
    setTargetCurrency(currency);
  };

  const handleConvert = () => {
    if (amount <= 0) {
      alert("Invalid Input");
    } else if (baseCurrency === null && targetCurrency === null) {
      alert("Please Choose Currency");
    } else if (baseCurrency === null || targetCurrency === null) {
      alert("Please Choose Currency");
    } else {
      const code = baseCurrency.code;

      fetch("https://api.exchangeratesapi.io/latest?base=" + code)
        .then(res => res.json())
        .then(res => {
          const targetCode = targetCurrency.code;

          const rate = res.rates[targetCode];

          // console.log(rate)
          // this.setState({convertedAmount:this.state.amount*rate})
          setConvertedAmount(amount * rate);
          // this.setState({convertedCode:targetCode})
          setConvertedCode(targetCode);
        });
    }
  };

  return (
    <div style={{ width: "70%" }}>
      <h1 className="text-center my-5">Forex Calculator</h1>
      {showTable === true ? (
        <div>
          <h2 className="text-center">
            Exhange Rate for: {baseCurrency.currency}
          </h2>
          <div className="d-flex justify-content-center">
            <ForexTable rates={rates} />
          </div>
        </div>
      ) : (
        ""
      )}
      <div
        className="d-flex justify-content-around"
        style={{ margin: "0 200px" }}
      >
        <ForexDropdown
          label={"Base Currrency"}
          onClick={handleBaseCurrency}
          currency={baseCurrency}
        />
        <ForexDropdown
          label={"Target Currrency"}
          onClick={handleTargetCurrency}
          currency={targetCurrency}
        />
      </div>
      <div className="d-flex justify-content-around">
        <ForexInput
          label={"Amount"}
          placeholder={"Amout to Convert"}
          onChange={handleAmount}
        />
        <Button color="info" onClick={handleConvert}>
          Convert
        </Button>
        <div>
          <h1 className="text-center">
            {convertedAmount} {convertedCode ? convertedCode : ""}{" "}
          </h1>
        </div>
      </div>
      {/* <div style={{maxHeight: "40vh", overflowY: "auto"}}>
                <Rates 
                    rates = {rates}
                    />
            </div> */}
    </div>
  );
};

export default Forex;

// Class

// import React, { Component } from 'react';
// import ForexDropdown from './ForexDropdown'
// import ForexInput from './ForexInput'
// import Rates from './Rates'
// import {Button} from 'reactstrap'

// class Forex extends Component {

//     state ={
//         amount: 0,
//         baseCurrency: null,
//         targetCurrency:null,
//         convertedAmount:0,
//         convertedCode:null,
//         rates:{}
//     }

//     handleAmount = e =>{
//         this.setState({
//             amount:e.target.value
//         })
//     }

//     handleBaseCurrency = currency =>{
//         this.setState({
//             baseCurrency: currency
//         })
//         const code = currency.code;
//         // console.log(currency)
//             fetch('https://api.exchangeratesapi.io/latest?base=' + code)
//             .then(res => res.json())
//             .then(res=>{
//             this.setState({ rates: res.rates  });

//         })

//     }

//     handleTargetCurrency = currency =>{
//         this.setState({
//             targetCurrency: currency
//         })
//     }

//     handleConvert = () => {

//         if(this.state.amount <= 0){
//             alert('Invalid Input')
//         }else if(this.state.baseCurrency === null && this.state.targetCurrency === null ){
//             alert('Please Choose Currency')
//         }else if(this.state.baseCurrency === null || this.state.targetCurrency === null){
//             alert('Please Choose Currency')
//         }else {
//             const code = this.state.baseCurrency.code;

//             fetch('https://api.exchangeratesapi.io/latest?base=' + code)
//             .then(res => res.json())
//             .then(res=>{

//             const targetCode = this.state.targetCurrency.code

//             const rate = res.rates[targetCode]

//             // console.log(rate)
//             this.setState({convertedAmount:this.state.amount*rate})
//             this.setState({convertedCode:targetCode})
//         })
//         }

//     }

//     render() {
//         return (
//             <div style={{width: "70%"}}>
//                 <h1 className='text-center my-5'>Forex Calculator</h1>
//                 <div className="d-flex justify-content-around"
//                     style={{margin:'0 200px'}}
//                 >
//                     <ForexDropdown
//                         label ={'Base Currrency'}
//                         onClick={this.handleBaseCurrency}
//                         currency = {this.state.baseCurrency}

//                      />
//                      <ForexDropdown
//                         label ={'Target Currrency'}
//                         onClick={this.handleTargetCurrency}
//                         currency ={this.state.targetCurrency}
//                      />
//                 </div>
//                 <div className="d-flex justify-content-around">
//                     <ForexInput
//                         label = {'Amount'}
//                         placeholder ={'Amout to Convert'}
//                         onChange = {this.handleAmount}

//                      />
//                      <Button
//                      color = "info"
//                      onClick = {this.handleConvert}
//                      >
//                      Convert
//                      </Button>
//                      <div>
//                          <h1 className = 'text-center'>{this.state.convertedAmount} {this.state.convertedCode ?this.state.convertedCode :""} </h1>
//                      </div>
//                 </div>
//                 <div style={{maxHeight: "40vh", overflowY: "auto"}}>
//                     <Rates
//                         rates = {this.state.rates}
//                      />
//                 </div>

//             </div>
//          );
//     }
// }

// export default Forex;
